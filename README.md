# richMap

RichMap is a port scanner and network host lookup tool written in Python 3.6 using MVC design pattern and TKinter framework. It offers user a variety of port scanning and network searching methods. RichMap allows user to launch it through command line, or using the GUI. GUI incorporates also few user-friendly features, like automatically adding discovered IPs to the host list which allows user to chose the scan target by selecting entry in the host list, or scan type list which automatically provides needed arguments for the command line entry.

### Port Scans
- TCP Scan
- SYN Scan
- UDP Scan
- ACK Scan
- FIN Scan
- XMas Scan
- Null Scan
- Window Scan
- Maimon's Scan

### Network Scans
- Ping Scan
- ARP Scan
- ARP Stealth Scan


### Screenshots
![alt text](https://i.imgur.com/UxDr9FN.png)
- *CLI*

![alt text](https://i.imgur.com/h6Ef8f1.png)
- *GUI with high verbosity*
